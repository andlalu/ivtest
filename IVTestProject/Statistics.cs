﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IVTestProject
{
	class Statistics
	{
		#region Public Methods

		/// <summary>
		/// The mean.
		/// </summary>
		/// <param name="x">
		/// The x.
		/// </param>
		public static double Mean(List<double> x)
		{
			if (!x.Any())
			{
				throw new ArgumentException("Empty array in mean calculation");
			}

			var n = x.Count;
			var sum = x.Sum(elem => elem);
			return sum / n;
		}


		/// <summary>
		/// The variance.
		/// </summary>
		/// <param name="x">
		/// The x.
		/// </param>
		public static double Variance(List<double> x)
		{
			if (!x.Any())
			{
				throw new ArgumentException("Empty array in variance calculation");
			}

			var n = x.Count;
			var sumSqDifference = x.Sum(elem => (elem * elem));
			return sumSqDifference / n;
		}

		/// <summary>
		/// The weighted average.
		/// </summary>
		/// <param name="x">
		/// The x.
		/// </param>
		/// <param name="noOfIntervals">
		/// The number of intervals.
		/// </param>
		public static List<double> Ewma(List<double> x, int noOfIntervals)
		{
			if (x == null || !x.Any())
			{
				throw new ArgumentNullException(nameof(x));
			}

			if (x.Count < noOfIntervals)
			{
				throw new Exception("Insufficient observations to compute ewma values");
			}

			var alpha = EwmaWeight(noOfIntervals);

			if (alpha <= 0 || alpha >=1)
			{
				throw new Exception("Invalid noOfIntervals value");
			}

			var ewma = new List<double> {x[0]};

			for (var i = 1; i < x.Count; i++)
			{
				ewma.Add((1-alpha) * ewma[i-1] + alpha * x[i]);
			}

			return ewma;
		}

		/// <summary>
		/// The Ewma alpha weight.
		/// </summary>
		public static double EwmaWeight(int noOfIntervals)
		{
			return 2.0 / (noOfIntervals + 1.0);
		}

		#endregion
	}
}

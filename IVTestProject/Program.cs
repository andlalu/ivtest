﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using IVTestProject.SupportClasses;

namespace IVTestProject
{
	internal class Program
	{
		private static void Main(string[] args)
		{
			// program parameters
			var spotFilePath = Path.Combine(Environment.CurrentDirectory, "..//..//..//RawData//usd_prices_1minte.csv");
			var returnFilePath =
				Path.Combine(Environment.CurrentDirectory, "..//..//..//RawData//usd_returns_1hour.csv");
			const int priceNormalisationWindow = 200;
			const int n1 = 15 * 24;
			const int n2 = 60 * 24;
			var downSampleFrequency = Data.Frequency.Hourly;
			const double targetExpectedReturn = 5e4;

			// reading Data
			Console.WriteLine("Reading raw spot data...");

			var errors = new Stack<string>();
			if (!Data.TryReadSpotCsvFile(
				spotFilePath,
				priceNormalisationWindow,
				downSampleFrequency,
				out var spots,
				out var rollingVar,
				out var highs,
				out var lows,
				ref errors))
			{
				Console.WriteLine($"Error encountered when reading spot data: {errors.Pop()}");
				Console.ReadLine();
			}

			Console.WriteLine("Done.");

			// reading Data
			Console.WriteLine("Reading hourly return data...");

			if (!Data.TryReadReturnCSVFile(
				returnFilePath,
				ref errors,
				out var dollarUnitReturns))
			{
				Console.WriteLine($"Error encountered when reading return data: {errors.Pop()}");
				Console.ReadLine();
			}

			Console.WriteLine("Done.");

			// compute the trading signals without re-scaling
			Console.WriteLine("Computing regular trading signals...");
			var regularTradingSignals = TradingSignalFactory.GetTradingSignals(spots, n1, n2);
			Console.WriteLine("Done.");

			// compute the trading signals using scaled return series
			Console.WriteLine("Computing trading signals based on scaled returns...");
			var scaledSpots = new Dictionary<DateTime, double>();
			var normalisedPrice = 0.0;
			for (int i = 1; i < spots.Count; ++i)
			{
				var spot = spots.ElementAt(i);
				var spotM1 = spots.ElementAt(i - 1);
				if (!rollingVar.TryGetValue(spot.Key, out var variance))
				{
					continue;
				}

				normalisedPrice += (spot.Value - spotM1.Value) /  Math.Sqrt(variance);
				scaledSpots.Add(spot.Key, normalisedPrice);
			}

			var tradingSignalsUsingScaledSpots = TradingSignalFactory.GetTradingSignals(scaledSpots, n1, n2);
			Console.WriteLine("Done.");

			// find absolute dollar returns 
			Console.WriteLine("Computing dollar returns using trading signals...");
			var dollarReturnsRegularTradingSignal =
				ReturnCalculator.ComputeDollarReturns(regularTradingSignals, dollarUnitReturns, true);
			var dollarReturnsScaledTradingSignal =
				ReturnCalculator.ComputeDollarReturns(tradingSignalsUsingScaledSpots, dollarUnitReturns, true);
			Console.WriteLine("Done.");

			// find position scaling (naive ergodicity assumption E[\Delta P] -> plim to mean A. Hence E[Scaling * \Delta P] -> plim 50K iff Scaling = 50K/A)
			var dollarScalingFactorRegularTradingSignal =
				targetExpectedReturn / (24 * Statistics.Mean(dollarReturnsRegularTradingSignal.Values.ToList()));
			Console.WriteLine(
				$"Scaling factor for regular trading signal {dollarScalingFactorRegularTradingSignal.ToString(CultureInfo.InvariantCulture)}");
			var dollarScalingFactorScaledTradingSignal =
				targetExpectedReturn / (24 * Statistics.Mean(dollarReturnsScaledTradingSignal.Values.ToList()));
			Console.WriteLine(
				$"Scaling factor for scaled trading signal {dollarScalingFactorScaledTradingSignal.ToString(CultureInfo.InvariantCulture)}");

			// publish csv with strategy signals, returns and cumulative returns
			Console.WriteLine("Publishing csv output files...");

			var regularSignalCsv = new List<string>();
			regularSignalCsv.Add(
				"Timestamp, Regular Signal, Regular Return, Cumulative Regular Return, Scaled Regular Return, Cumulative Scaled Regular Return");
			var cumulativeRegularReturn = 0.0;
			foreach (var item in regularTradingSignals)
			{
				var regularReturn = "";
				if (dollarReturnsRegularTradingSignal.TryGetValue(item.Key, out var value))
				{
					regularReturn = value.ToString(CultureInfo.InvariantCulture);
					cumulativeRegularReturn += value;
				}

				regularSignalCsv.Add($"{item.Key.ToString("yyyy-MM-dd HH:mm:sszzz", CultureInfo.InvariantCulture)}," +
				                     $"{item.Value.ToString()}," +
				                     $"{regularReturn}," +
				                     $"{cumulativeRegularReturn.ToString(CultureInfo.InvariantCulture)}," +
				                     $"{value * dollarScalingFactorRegularTradingSignal}," +
				                     $"{cumulativeRegularReturn * dollarScalingFactorRegularTradingSignal}");
			}

			Data.PublishCsvFile(Path.Combine(Environment.CurrentDirectory, "..//..//..//RegularTradingStrategy.csv"),
				regularSignalCsv, ref errors);

			var scaledSignalCsv = new List<string>();
			scaledSignalCsv.Add(
				"Timestamp, Signal, Return, Cumulative Return, Scaled Return, Cumulative Scaled Return");
			var cumulativeScaledReturn = 0.0;
			foreach (var item in tradingSignalsUsingScaledSpots)
			{
				var ret = "";
				if (dollarReturnsScaledTradingSignal.TryGetValue(item.Key, out var value))
				{
					ret = value.ToString(CultureInfo.InvariantCulture);
					cumulativeScaledReturn += value;
				}

				scaledSignalCsv.Add($"{item.Key.ToString("yyyy-MM-dd HH:mm:sszzz", CultureInfo.InvariantCulture)}," +
				                    $"{item.Value.ToString()}," +
				                    $"{ret}," +
				                    $"{cumulativeScaledReturn.ToString(CultureInfo.InvariantCulture)}," +
				                    $"{value * dollarScalingFactorScaledTradingSignal}," +
				                    $"{cumulativeScaledReturn * dollarScalingFactorScaledTradingSignal}");
			}

			Data.PublishCsvFile(Path.Combine(Environment.CurrentDirectory, "..//..//..//ScaledTradingStrategy.csv"),
				scaledSignalCsv, ref errors);

			Console.WriteLine("Finished.");
			Console.ReadLine();

			// TODO:
			// write unit tests.
		}
	}
}
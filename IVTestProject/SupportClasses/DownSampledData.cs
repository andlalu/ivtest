﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IVTestProject.SupportClasses
{

	/// <summary>
	/// The DownSampledDataObservation
	/// </summary>
	class DownSampledData
	{
		/// <summary>
		/// The downsampled Spot.
		/// </summary>
		public SampledData Spot;

		/// <summary>
		/// The downsampled High.
		/// </summary>
		public SampledData High;

		/// <summary>
		/// The downsample Low.
		/// </summary>
		public SampledData Low;

	}
}

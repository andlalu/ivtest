﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IVTestProject.SupportClasses
{

	/// <summary>
	/// The downsampled observation class.
	/// </summary>
	class SampledData
	{
		#region Public members;

		/// <summary>
		/// The observation's timestamp.
		/// </summary>
		public DateTime ObservationTimeStamp;

		/// <summary>
		/// The observation's Value;
		/// </summary>
		public double Value;

		#endregion

		#region Constructors

		public SampledData(DateTime observationTimeStamp, double value)
		{
			this.ObservationTimeStamp = observationTimeStamp;
			this.Value = value;
		}

		#endregion


	}
}

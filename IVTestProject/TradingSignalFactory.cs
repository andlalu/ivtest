﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;

namespace IVTestProject
{
	/// <summary>
	/// The trading signal factory class.
	/// </summary>
	class TradingSignalFactory
	{
		#region PublicMethods

		/// <summary>
		/// Gets trading
		/// </summary>
		/// <param name="spotSeries">
		/// The spot series used to get trading signals.
		/// </param>
		/// <param name="noOfIntervalsBuySignal">
		/// The n1 signal.
		/// </param>
		/// <param name="noOfIntervalsSellSignal">
		/// The n2 signal.
		/// </param>
		public static Dictionary<DateTime, int> GetTradingSignals(Dictionary<DateTime, double> spotSeries, int noOfIntervalsBuySignal, int noOfIntervalsSellSignal)
		{
			var ewmaN1 = Statistics.Ewma(spotSeries.Values.ToList(), noOfIntervalsBuySignal);
			var ewmaN2 = Statistics.Ewma(spotSeries.Values.ToList(), noOfIntervalsSellSignal);

			return spotSeries.ToList()
				.Select((q,
					i) => new KeyValuePair<DateTime, int>(q.Key,
					ewmaN1[i] > ewmaN2[i]
						? 1
						: -1))
				.ToDictionary(q => q.Key,
					q => q.Value);
		}
		
		#endregion

	}
}

﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using GenericParsing;
using IVTestProject.SupportClasses;

namespace IVTestProject
{
	/// <summary>
	/// Data methods.
	/// </summary>
	public class Data
	{
		#region Enums

		/// <summary>
		/// The data read frequency
		/// </summary>
		public enum Frequency
		{
			Hourly = 0,
			Daily = 1,
		}

		#endregion

		#region Public Methods

		/// <summary>
		/// Reads timestamped close prices from a csv file and calculates rolling standard deviations and other statistics.
		/// </summary>
		/// <param name="filePath">
		/// The file path.
		/// </param>
		/// <param name="readErrors">
		/// The read errors.
		/// </param>
		/// <param name="hasHeader">
		/// Flag indicating whether the file has a header or not.
		/// </param>
		/// <param name="priceNormalisationWindow">
		/// The rolling variance calculation window length.
		/// </param>
		/// <param name="downsampleFrequency">
		/// The downsample frequency
		/// </param>
		/// <param name="spots">
		/// The dictionary of spots by reference time stamps.
		/// </param>
		/// <param name="rollingVar">
		/// The dictionary of rolling variances by reference time stamps.
		/// </param>
		/// <param name="highs">
		/// The dictionary of highs by reference time stamps.
		/// </param>
		/// <param name="lows">
		/// The dictionary of lows by reference time stamps.
		/// </param>
		/// <param name="timeStampFormat">
		/// The timestamp string format
		/// </param>
		/// <returns>{bool} True if parsing was successful, false otherwise.</returns>
		public static bool TryReadSpotCsvFile(
			string filePath,
			int priceNormalisationWindow,
			Frequency downsampleFrequency,
			out Dictionary<DateTime, double> spots,
			out Dictionary<DateTime, double> rollingVar,
			out Dictionary<DateTime, double> highs,
			out Dictionary<DateTime, double> lows,
			ref Stack<string> readErrors,
			bool hasHeader = false,
			string timeStampFormat = "yyyy-MM-dd HH:mm:sszzz")
		{
			spots = new Dictionary<DateTime, double>();
			rollingVar = new Dictionary<DateTime, double>();
			highs = new Dictionary<DateTime, double>();
			spots = new Dictionary<DateTime, double>();
			lows = new Dictionary<DateTime, double>();

			if (!ValidateCsvFilePath(filePath, ref readErrors))
			{
				return false;
			}

			if (priceNormalisationWindow <= 2)
			{
				readErrors.Push("Invalid price normalisation window");
				return false;
			}

			try
			{
				var parseDictionary = new Dictionary<DateTime, DownSampledData>();
				using (GenericParser parser = new GenericParser())
				{
					parser.SetDataSource(filePath);
					parser.FirstRowHasHeader = hasHeader;
					parser.MaxRows = 0;

					while (parser.Read())
					{
						if (DateTime.TryParseExact(
							    parser[0],
							    timeStampFormat,
							    null,
							    DateTimeStyles.None,
							    out var currentTimeStamp)
						    && TryValidateSpot(parser[1],
							    out var spot))
						{

							var referenceTimeStamp =
								GetReferenceTimeStampFromFullTimeStamp(downsampleFrequency, currentTimeStamp);
							if (!parseDictionary.TryGetValue(referenceTimeStamp,
								out var existingDownsampledObservation))
							{
								parseDictionary.Add(referenceTimeStamp, new DownSampledData
								{
									Spot = new SampledData(currentTimeStamp, spot),
									High = new SampledData(currentTimeStamp, spot),
									Low = new SampledData(currentTimeStamp, spot)
								});
							}
							else
							{
								if (existingDownsampledObservation.Spot.ObservationTimeStamp < currentTimeStamp)
								{
									existingDownsampledObservation.Spot = new SampledData(currentTimeStamp, spot);
								}

								if (existingDownsampledObservation.High.Value < spot)
								{
									existingDownsampledObservation.High = new SampledData(currentTimeStamp, spot);
								}

								if (existingDownsampledObservation.Low.Value > spot)
								{
									existingDownsampledObservation.Low = new SampledData(currentTimeStamp, spot);
								}

								parseDictionary[referenceTimeStamp] = existingDownsampledObservation;
							}
						}
					}

					spots = parseDictionary.ToDictionary(q => q.Key, q => q.Value.Spot.Value);
					lows = parseDictionary.ToDictionary(q => q.Key, q => q.Value.Low.Value);
					highs = parseDictionary.ToDictionary(q => q.Key, q => q.Value.High.Value);

					// deal with rolling variance
					if (parseDictionary.Count < priceNormalisationWindow + 1)
					{
						readErrors.Push("Too few observations for price normalisation.");
					}

					var orderedSpots = spots.OrderBy(q => q.Key).ToList();

					for (int i = priceNormalisationWindow+1; i < orderedSpots.Count; ++i)
					{
						var tm1Series = orderedSpots
							.Skip(i - priceNormalisationWindow-1)
							.Take(priceNormalisationWindow).ToList();

						var diffSeries =  orderedSpots
							.Skip(i - priceNormalisationWindow)
							.Take(priceNormalisationWindow).Select((q, i) => q.Value-tm1Series[i].Value).ToList();

						rollingVar[orderedSpots[i].Key] = Statistics.Variance(diffSeries);
					}
				}

				return true;
			}
			catch (Exception ex)
			{
				readErrors.Push(ex.Message);
			}

			return false;
		}

		/// <summary>
		/// Reads a return formatted CSV file.
		/// </summary>
		/// <param name="filePath">
		/// The csv file path.
		/// </param>
		/// <param name="readErrors">
		/// The read error message stack.
		/// </param>
		/// <param name="dollarReturns">
		/// The dictionary of dollar returns by timestamps.
		/// </param>
		/// <param name="hasHeader">
		/// The has header flag.
		/// </param>
		/// <param name="timeStampFormat">
		/// The timestamp format.
		/// </param>
		/// <returns></returns>
		public static bool TryReadReturnCSVFile(
			string filePath,
			ref Stack<string> readErrors,
			out Dictionary<DateTime, double> dollarReturns,
			bool hasHeader = false,
			string timeStampFormat = "yyyy-MM-dd HH:mm:sszzz")
		{
			dollarReturns = new Dictionary<DateTime, double>();

			if (!ValidateCsvFilePath(filePath, ref readErrors))
			{
				return false;
			}

			try
			{
				using (GenericParser parser = new GenericParser())
				{
					parser.SetDataSource(filePath);
					parser.FirstRowHasHeader = hasHeader;
					parser.MaxRows = 0;

					while (parser.Read())
					{
						if (DateTime.TryParseExact(
							    parser[0],
							    timeStampFormat,
							    null,
							    DateTimeStyles.None,
							    out var currentTimeStamp)
						    && TryValidateSpot(parser[1],
							    out var dollarReturn))
						{
							dollarReturns.Add(currentTimeStamp, dollarReturn);
						}

					}

					return true;
				}
			}
			catch (Exception ex)
			{
				readErrors.Push(ex.Message);
			}

			return false;
		}

		/// <summary>
		/// Publishes a csv data file. 
		/// </summary>
		/// <param name="filePath">
		/// The file path.
		/// </param>
		/// <param name="csvFileRows">
		/// The file content.
		/// </param>
		/// <param name="errorMessages">
		/// The error messages.
		/// </param>
		public static bool PublishCsvFile(string filePath, List<string> csvFileRows, ref Stack<string> errorMessages)
		{
			try
			{
				var csv = new StringBuilder();
				csvFileRows.ForEach(q => csv.AppendLine(q));
				File.WriteAllText(filePath, csv.ToString());
			}
			catch (Exception e)
			{
				errorMessages.Push(e.Message);
			}
			
			return false;
		}

		#endregion

		#region Private methods

		/// <summary>
		/// Validates the csv file path.
		/// </summary>
		/// <param name="filePath">
		/// The file path.
		/// </param>
		/// <param name="errorMessages">
		/// The error message stack to write to in case the file path is not valid.
		/// </param>
		/// <returns>True if file path is valid, false otherwise.</returns>
		private static bool ValidateCsvFilePath(string filePath, ref Stack<string> errorMessages)
		{
			// some basic file path validation
			if (string.IsNullOrEmpty(filePath) || !filePath.EndsWith(".csv"))
			{
				errorMessages.Push("Invalid file path");
				return false;
			}

			return true;
		}

		/// <summary>
		/// Validates Spot reading.
		/// </summary>
		/// <param name="spotString">
		/// The Spot string.
		/// </param>
		/// <param name="spot">
		/// The Spot Value.
		/// </param>
		/// <returns>
		/// True if valid Spot Value was interpreted, false otherwise.
		/// </returns>
		private static bool TryValidateSpot(string spotString, out double spot)
		{
			spot = double.NaN;
			return !string.IsNullOrEmpty(spotString)
			       && double.TryParse(spotString, out spot)
			       && spot > 0;
		}

		/// <summary>
		/// Gets the reference time stamp from the full time stamp for a given downsampling frequency.
		/// </summary>
		/// <param name="downsamplingFrequency">
		/// The downsampling frequency.
		/// </param>
		/// <param name="fullTimeStamp">
		/// The full time stamp.
		/// </param>
		/// <returns></returns>
		private static DateTime GetReferenceTimeStampFromFullTimeStamp(Frequency downsamplingFrequency, DateTime fullTimeStamp)
		{
			switch (downsamplingFrequency)
			{
				case Frequency.Daily:
					return new DateTime(fullTimeStamp.Year, fullTimeStamp.Month, fullTimeStamp.Day, 0, 0, 0);
				case Frequency.Hourly:
					return new DateTime(fullTimeStamp.Year, fullTimeStamp.Month, fullTimeStamp.Day, fullTimeStamp.Hour, 0, 0);
				default:
					throw new NotImplementedException("Unknown downsampling frequency");
			}
		}

		
		#endregion
	}
}
